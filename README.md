# UKLotto #

A simple API for querying the UK Lotto results database. This API is hosted on 
[http://lottoapi.kodefoundry.io](http://lottoapi.kodefoundry.io)

## Routes

### Lucky Dip

This route will give you a random set of numbers to play with

    GET /luckydip
    
### Result

Check a result by specifying the game week number

    GET /result/:id
    
### Stats

Check the stats for a given number

    GET /stats/:number
    
### Query

Query all results where a number appears in any of the balls (including bonus ball)

    GET /query
    
The following query parameters search for all the results where numbers 1 and 12 appears

    GET /query?balls=1,12
    
You can optionally pass in the ``page`` query parameter. Results are paginated by 10.
    

