import io.kodefoundry.lotto._
import org.scalatra._
import javax.servlet.ServletContext
import io.kodefoundry.lotto.db.DatabaseInit

class ScalatraBootstrap extends LifeCycle with DatabaseInit {

  override def init(context: ServletContext) {
    configureDb()
    context.mount(new UKLottoServlet, "/*")
  }

  override def destroy(context: ServletContext) {
    closeDbConnection()
  }
}
