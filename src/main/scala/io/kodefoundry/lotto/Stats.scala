package io.kodefoundry.lotto

import com.google.gson.Gson
import io.kodefoundry.lotto.db.Library
import org.squeryl.PrimitiveTypeMode._


trait JsonSupport {
  /**
   * Return a serialised object as a JSON string
   * @return
   */
  def toLottoJson: String = (new Gson).toJson(this)
}


class Frequency(val num: Int) extends JsonSupport {

  var b1_count: Int = 0
  var b2_count: Int = 0
  var b3_count: Int = 0
  var b4_count: Int = 0
  var b5_count: Int = 0
  var b6_count: Int = 0
  var bonus_count: Int = 0
  var total: Int = 0
  var frequency: Float = 0

  inTransaction {
    b1_count = Library.results.count(_.ball_1 == num)
    b2_count = Library.results.count(_.ball_2 == num)
    b3_count = Library.results.count(_.ball_3 == num)
    b4_count = Library.results.count(_.ball_4 == num)
    b5_count = Library.results.count(_.ball_5 == num)
    b6_count = Library.results.count(_.ball_6 == num)
    bonus_count = Library.results.count(_.bonus_ball == num)
    total = b1_count + b2_count + b3_count + b4_count + b5_count + b6_count + bonus_count
    frequency = total.toFloat / Library.results.count(_ => true) * 100
  }

}

object Stats {

  implicit def numberToFrequency(n: Int): Frequency = new Frequency(n)
}