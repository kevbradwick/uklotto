package io.kodefoundry.lotto

import org.scalatra.CorsSupport
import io.kodefoundry.lotto.db.DatabaseSessionSupport
import io.kodefoundry.lotto.db.NumberHelpers
import io.kodefoundry.lotto.db.Library
import io.kodefoundry.lotto.Stats._
import org.squeryl.PrimitiveTypeMode._
import com.google.gson.Gson


class UKLottoServlet extends UklottoStack
  with DatabaseSessionSupport
  with NumberHelpers
  with CorsSupport {

  get("/") {
    contentType = "text/html"

    jade("/WEB-INF/templates/views/index.jade")
  }

  /**
   * Get the results for a game week.
   */
  get("""^\/result\/(\d+)""".r) {

    contentType = "application/json"

    inTransaction {
      val id: Int = multiParams("captures").seq(0).toInt
      try {
        val result = Library.results.where(_.draw_num === id).single
        val gson = new Gson()
        gson.toJson(result)
      } catch {
        case e: java.lang.RuntimeException => "{}"
      }
    }
  }

  /**
   * Generate a lucky dip
   */
  get("/luckydip") {

    contentType = "application/json"

    (new Gson).toJson((1 to 49).toLuckyDip)
  }

  /**
   * Query the results in the db.
   *
   * Allowed query parameters
   *  - balls: list of numbers to search with
   *  - page: page number (default 1)
   */
  get("/query") {

    contentType = "application/json"

    val balls: Seq[Int] = params.getOrElse("balls", "") split "," map {
      _.toInt
    }

    val gson = new Gson

    // paginate by 10
    val offset: Int = params.getOrElse("page", "1").toInt * 10 - 10

    if (!balls.isEmpty) {

      inTransaction {
        // check all columns that contain the balls
        val results = Library.results.where({ r =>
          (r.ball_1 in balls) or
          (r.ball_2 in balls) or
          (r.ball_3 in balls) or
          (r.ball_4 in balls) or
          (r.ball_5 in balls) or
          (r.ball_6 in balls) or
          (r.bonus_ball in balls)
        })

        gson.toJson(results.page(offset, 10) toArray)
      }
    } else {
      gson.toJson(List())
    }
  }

  /**
   * Find out information about a number e.g. how many times it was drawn
   */
  get("""^\/stats\/(\d{1,2})$""".r) {

    contentType = "application/json"

    val num: Int = multiParams("captures").seq(0).toInt
    num match {
      case 0 => "{}" // should really 404 here
      case _ => num.toLottoJson
    }
  }
}
