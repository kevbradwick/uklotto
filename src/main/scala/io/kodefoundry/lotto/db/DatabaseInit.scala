package io.kodefoundry.lotto.db

import com.mchange.v2.c3p0.ComboPooledDataSource
import org.squeryl.adapters.{H2Adapter, MySQLAdapter}
import org.squeryl.Session
import org.squeryl.SessionFactory
import org.slf4j.LoggerFactory


trait DatabaseInit {

  val logger = LoggerFactory.getLogger(getClass)

  val dbUser = "root"
  val dbPass = ""
  val dbConn = "jdbc:mysql://localhost:3306/lotto_results?autoReconnect=true"

  val cpds = new ComboPooledDataSource

  def configureDb() = {

    cpds.setDriverClass("com.mysql.jdbc.Driver")
    cpds.setJdbcUrl(dbConn)
    cpds.setUser(dbUser)
    cpds.setPassword(dbPass)

    cpds.setMinPoolSize(1)
    cpds.setAcquireIncrement(1)
    cpds.setMaxPoolSize(50)

    SessionFactory.concreteFactory = Some(() => connection())

    def connection() = {
      logger.info("Creating connection with C3P0 pool")
      Session.create(cpds.getConnection, new H2Adapter)
    }
  }

  def closeDbConnection() {
    logger.info("Closing c3po connection pool")
    cpds.close()
  }
}
