package io.kodefoundry.lotto.db

import org.squeryl.Schema
import java.util.Date
import scala.util.Random


class Result(val id: Long, val draw_num: Int, val ball_set: Int, val machine_name: String,
              val ball_1: Int, val ball_2: Int, val ball_3: Int, val ball_4: Int, val ball_5: Int,
              val ball_6: Int, val bonus_ball: Int, val draw_date: Date) {}

object Library extends Schema {

  var results = table[Result]("uk_lotto")

}

trait NumberHelpers {

  class RangeUtil(val range: Range) {

    def toLuckyDip: Array[Int] = Random.shuffle(this.range toList).take(6).toArray

  }

  implicit def rangeToRangeUtil(range: Range): RangeUtil = new RangeUtil(range)
}